package com.jahangir.fileuploadapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.jahangir.fileuploadapi.ws.storage.StorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({
	StorageProperties.class
})
public class FileUploadApiDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileUploadApiDemoApplication.class, args);
	}

}
