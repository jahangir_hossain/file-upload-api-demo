package com.jahangir.fileuploadapi.ws.shared.Dto;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadDto {
	
	private String title;
	private String description;
	private MultipartFile[] files;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MultipartFile[] getFiles() {
		return files;
	}

	public void setFiles(MultipartFile[] files) {
		this.files = files;
	}

}
