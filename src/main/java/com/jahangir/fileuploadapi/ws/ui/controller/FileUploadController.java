package com.jahangir.fileuploadapi.ws.ui.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.jahangir.fileuploadapi.ws.storage.StorageService;
import com.jahangir.fileuploadapi.ws.ui.RequestModel.FileRequestModel;
import com.jahangir.fileuploadapi.ws.ui.ResponseModel.UploadResponseModel;

@RestController
@RequestMapping(value = "files")
@CrossOrigin
public class FileUploadController {

	@Autowired
	StorageService storageService;

	@GetMapping("/{fileName}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
		// Load file as Resource
		Resource resource = storageService.loadAsResource(fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			throw new RuntimeException("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}
//	{ "multipart/form-data" }
	@PostMapping(consumes = { "multipart/form-data" }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public List<UploadResponseModel> upload(FileRequestModel fileRequestModel) {

		List<UploadResponseModel> uploadedFiles = new ArrayList<UploadResponseModel>();
		for (MultipartFile file : fileRequestModel.getFiles()) {
			String fileName = storageService.store(file);
//			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//	                .path("/downloadFile/")
//	                .path(fileName)
//	                .toUriString();

			String path = UriComponentsBuilder.newInstance().path("/downloadFile/").path(fileName).build()
					.toUriString();
			uploadedFiles.add(new UploadResponseModel(fileName, path, file.getContentType(), file.getSize()));
		}
		return uploadedFiles;
	}

	@DeleteMapping("/{fileName}")
	public String deleteByFileName(@PathVariable String fileName) {
		storageService.deleteByFileName(fileName);
		return "OK";

	}

}
