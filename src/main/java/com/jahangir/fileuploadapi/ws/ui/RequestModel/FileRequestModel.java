package com.jahangir.fileuploadapi.ws.ui.RequestModel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;


public class FileRequestModel {
	private String name;
	private String type;
	
	private MultipartFile[] files;
	private List<TransactionRequestModel> transactions;
	
	
	
	

	public MultipartFile[] getFiles() {
		return files;
	}

	public void setFiles(MultipartFile[] files) {
		this.files = files;
	}

	public List<TransactionRequestModel> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionRequestModel> transactions) {
		this.transactions = transactions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
}
