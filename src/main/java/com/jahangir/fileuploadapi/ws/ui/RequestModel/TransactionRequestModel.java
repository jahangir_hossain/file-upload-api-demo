package com.jahangir.fileuploadapi.ws.ui.RequestModel;

import java.util.Date;
import java.util.List;

public class TransactionRequestModel {
	private String description;
	private Date date;
	private List<String> compensationAmount;
	private List<String> costAmount;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<String> getCompensationAmount() {
		return compensationAmount;
	}

	public void setCompensationAmount(List<String> compensationAmount) {
		this.compensationAmount = compensationAmount;
	}

	public List<String> getCostAmount() {
		return costAmount;
	}

	public void setCostAmount(List<String> costAmount) {
		this.costAmount = costAmount;
	}
	
	



}
