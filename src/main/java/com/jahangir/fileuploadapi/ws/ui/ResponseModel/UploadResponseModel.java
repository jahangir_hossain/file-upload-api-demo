package com.jahangir.fileuploadapi.ws.ui.ResponseModel;

public class UploadResponseModel {

	private String fileName;
	private String path;
	private String fileType;
	private long size;

	public UploadResponseModel(String fileName, String path, String fileType, long size) {
		this.fileName = fileName;
		this.path = path;
		this.fileType = fileType;
		this.size = size;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

}
